const userModule = {
    namespaced: true,
    state: {
        username: "",
        stayLoggedIn: false,
        sessionToken: ""
    },
    mutations: {
        setUsername(state: any, username: string){
            state.username = username
        },
        setStayLoggedIn(state: any, stayLoggedIn: boolean){
            console.log("switch" + stayLoggedIn)
            state.stayLoggedIn = stayLoggedIn
        },
        setSessionToken(state: any, token: string){
            state.sessionToken = token
        }
    },
    actions: {
        init: function({ commit }: any) {
            let sessionToken = localStorage.getItem("sessionToken")
            if(sessionToken === null){
                sessionToken = "";
            }
            commit("setSessionToken", sessionToken)
        },
        login: async function ({ state, commit }: any, sessionToken: string) {
            if(state.stayLoggedIn){
                localStorage.setItem("sessionToken", sessionToken)
            }
            commit("setSessionToken", "anyToken")
        },
        logout: async function ({ state, commit }: any) {
            localStorage.removeItem("sessionToken")
            commit("setSessionToken", "")
        }
    },
    getters: {
        isLoggedIn: function (state: any): boolean {
            return state.sessionToken !== ""
        }
    }
}

export default userModule