export class LoginResponseModel {

    
    private readonly _success:           boolean;
    private readonly _accessToken:     string | undefined;

    constructor(success: boolean, accessToken: string | undefined) {
        this._success = success;
        this._accessToken = accessToken;
    }

    get success(): boolean {
        return this._success;
    }

    get accessToken(): string | undefined {
        return this._accessToken;
    }
}