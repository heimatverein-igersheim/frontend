import axios from 'axios';
import {ServerGateway} from "@/gateways/http/server.gateway";
import { LoginResponseModel } from '@/models/login-response.model';

export class UserGateway {

    constructor(private httpClient: ServerGateway) {}

    login(username: string, password: string): Promise<LoginResponseModel> {
        return new Promise<any>((resolve, reject) => {   
            this.httpClient.getHttpInstance().post('/login', {data: {username: username, password: password}}).then((response) => {
                resolve(new LoginResponseModel(response.data.success, response.data.accessToken))
            }).catch(reject);
        });
    }

}