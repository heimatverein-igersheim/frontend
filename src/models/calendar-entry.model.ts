export class CalendarEntryModel {

    private readonly _identifier:     number;
    private readonly _start:     Date;
    private readonly _end:       Date;
    private readonly _title: string;
    private readonly _description: string;
    private readonly _location: string;

    constructor(identifier: number, start: Date, end: Date, title: string, description: string, location: string) {
        this._identifier = identifier;
        this._start = start;
        this._end = end;
        this._title = title;
        this._description = description;
        this._location = location;
    }

    get identifier() {
        return this._identifier;
    }

    get start() {
        return this._start;
    }
    
    get end() {
        return this._end;
    }

    get title() {
        return this._title;
    }

    get description() {
        return this._description;
    }

    get location() {
        return this._location;
    }
}