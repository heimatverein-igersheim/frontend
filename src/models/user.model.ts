export class UserModel {

    private readonly _authToken:     string;
    private readonly _identifier:     number;
    private readonly _username:           string;

    constructor(authToken: string, identifier: number, username: string) {
        this._authToken = authToken;
        this._identifier     = identifier;
        this._username           = name;
    }

    get authToken(): string {
        return this._authToken;
    }

    get identifier(): number {
        return this._identifier;
    }

    get username(): string {
        return this._username;
    }
}