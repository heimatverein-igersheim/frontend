import axios from 'axios';
import {ServerGateway} from "@/gateways/http/server.gateway";
import { CalendarEntryModel } from '@/models/calendar-entry.model';

export class CalendarGateway {

    constructor(private httpClient: ServerGateway) {}

    addEntry(start: Date, end: Date, title: string, description: string, location: string): void {
        this.httpClient.getHttpInstance().post('/calendar/entry', {data: {start: start.toISOString(), end: end.toISOString(), title: title, description: description, location: location}});
    }

    getAllEntries(count: number): Promise<CalendarEntryModel[]> {
        return new Promise<any>((resolve, reject) => {   
            this.httpClient.getHttpInstance().get('/calendar/entries').then((response) => {
                const entries = [];
                
                for(const item of response.data) {
                    entries.push(new CalendarEntryModel(item.id, new Date(Date.parse(item.start) * 1000), new Date(Date.parse(item.end) * 1000), item.title, item.description, item.location));
                }

                resolve(entries);
            }).catch(reject);
        });
    }

    getNextEntries(count: number): Promise<CalendarEntryModel[]> {
        return new Promise<any>((resolve, reject) => {   
            this.httpClient.getHttpInstance().get('/calendar/entries', {data: {count: count}}).then((response) => {
                const entries = [];
                
                for(const item of response.data) {
                    entries.push(new CalendarEntryModel(item.id, new Date(Date.parse(item.start) * 1000), new Date(Date.parse(item.end) * 1000), item.title, item.description, item.location));
                }

                resolve(entries);
            }).catch(reject);
        });
    }

    getEntriesAt(month: number, year: number): Promise<CalendarEntryModel[]> {
        return new Promise<any>((resolve, reject) => {   
            this.httpClient.getHttpInstance().get('/calendar/entries', {data: {month: month, year: year}}).then((response) => {
                const entries = [];
                
                for(const item of response.data) {
                    entries.push(new CalendarEntryModel(item.id, new Date(Date.parse(item.start) * 1000), new Date(Date.parse(item.end) * 1000), item.title, item.description, item.location));
                }

                resolve(entries);
            }).catch(reject);
        });
    }

    deleteEntry(identifier: number): void {
        this.httpClient.getHttpInstance().delete('/calendar/entry', {data: {id: identifier}});
    }

}