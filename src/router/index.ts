import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import store from '@/store'

Vue.use(VueRouter)

  const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/verein',
    name: 'Verein',
    component: () => import(/* webpackChunkName: "about" */ '../views/Verein.vue')
  },
  {
    path: '/ausstellungen',
    name: 'Ausstellungen',
    component: () => import(/* webpackChunkName: "about" */ '../views/Ausstellungen.vue')
  },
  {
    path: '/bürgerbrotbacken',
    name: 'Buergerbrotbacken',
    component: () => import(/* webpackChunkName: "about" */ '../views/Buergerbrotbacken.vue')
  },
  {
    path: '/museum',
    name: 'Museum',
    component: () => import(/* webpackChunkName: "about" */ '../views/Museum.vue')
  },
  {
    path: '/bildstockwanderweg',
    name: 'Bildstockwanderweg',
    component: () => import(/* webpackChunkName: "about" */ '../views/Bildstockwanderweg.vue')
  },
  {
    path: '/krippenweg',
    name: 'Krippenweg',
    component: () => import(/* webpackChunkName: "about" */ '../views/Krippenweg.vue')
  },
  {
    path: '/gassenfest',
    name: 'Gassenfest',
    component: () => import(/* webpackChunkName: "about" */ '../views/Gassenfest.vue')
  },
  {
    path: '/kalender',
    name: 'Calendar',
    component: () => import(/* webpackChunkName: "about" */ '../views/Calendar.vue')
  },
  {
    path: '/datenschutzerklärung',
    name: 'Datenschutzerklaerung',
    component: () => import(/* webpackChunkName: "about" */ '../views/Datenschutzerklaerung.vue')
  },
  {
    path: '/impressum',
    name: 'Impressum',
    component: () => import(/* webpackChunkName: "about" */ '../views/Impressum.vue')
  },
  {
    path: '/admin/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "about" */ '../views/admin/Login.vue')
  },
  {
    path: '/admin/dashboard',
    name: 'Dashboard',
    meta: {
      requiredAuth: true
    },
    component: () => import(/* webpackChunkName: "about" */ '../views/admin/Dashboard.vue')
  },
  {
    path: '/admin/add-entry',
    name: 'AddEntry',
    meta: {
      requiredAuth: true
    },
    component: () => import(/* webpackChunkName: "about" */ '../views/admin/AddEntry.vue'),
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if(to.name === 'Logout'){
    // Notify the store about the logout
    store.dispatch("userModule/logout");
    next();
  } else if(to.matched.some(record => record.meta.requiresAuth)) {
      if (!store.getters['userModule/isLoggedIn']) {
          // Redirect to login when the user is not logged in
          next({
              path: '/admin/login',
              params: { nextUrl: to.fullPath }
          })
      } else {
        next()
      }
  }else {
      next()
  }
})

export default router
